import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import groovy.json.JsonSlurper as JsonSlurper
import org.assertj.core.api.Assertions as Assertions

'Send request to login, pre request to other TCs'
ResposeBody = WS.sendRequest(findTestObject('User_Token'))
WS.verifyResponseStatusCode(ResposeBody, 200)
Assertions.assertThat(ResposeBody.getStatusCode()).isEqualTo(200)

'convert the body to json'
JsonSlurper Parser = new JsonSlurper()
Map parsedJson = Parser.parseText(ResposeBody.getResponseText())

'Get the token from the body'
String access_token = parsedJson.access_token
println(access_token)


'Send the request to get the National address by ID'
Respose = WS.sendRequest(findTestObject('POST_NationalAddressById', [('Token') : access_token]))
WS.verifyResponseStatusCode(Respose, 200)

'Convert the body to json and print it'
def Body = Parser.parseText(Respose.getResponseBodyContent())
println(Body)

