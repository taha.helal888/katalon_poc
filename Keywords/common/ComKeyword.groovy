package common

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.sun.org.apache.bcel.internal.generic.RETURN

import groovy.json.JsonSlurper
import internal.GlobalVariable
import net.bytebuddy.asm.Advice.Return

public class ComKeyword {
	
	String access_token
	
	@Keyword
	public GetToken(){
		
		WS.sendRequest(findTestObject('User_Token'))
		JsonSlurper Parser = new JsonSlurper()
		Map parsedJson = Parser.parseText(WS.sendRequest(findTestObject('User_Token')).getResponseText())
		String access_token = parsedJson.access_token
		println(access_token)
		
		GlobalVariable.Token = access_token
		
		return access_token;
		
	}
	
	
	
}
